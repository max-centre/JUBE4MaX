#!/bin/bash
CPU_ID=$(cat /proc/self/stat | awk '{print $39}')
rank="$SLURM_LOCALID"

limit0=5
limit1=16
limit2=21
limit3=32
limit4=37
limit5=48
limit6=53
limit7=64

if [ $CPU_ID -lt $limit0 ]
then
   export ROCR_VISIBLE_DEVICES=4

elif [ $CPU_ID -lt $limit1 ]
then
   export ROCR_VISIBLE_DEVICES=5

elif [ $CPU_ID -lt $limit2 ]
then
   export ROCR_VISIBLE_DEVICES=2

elif [ $CPU_ID -lt $limit3 ]
then
   export ROCR_VISIBLE_DEVICES=3

elif [ $CPU_ID -lt $limit4 ]
then
   export ROCR_VISIBLE_DEVICES=6

elif [ $CPU_ID -lt $limit5 ]
then
   export ROCR_VISIBLE_DEVICES=7

elif [ $CPU_ID -lt $limit6 ]
then
   export ROCR_VISIBLE_DEVICES=0

else
   export ROCR_VISIBLE_DEVICES=1
fi

echo "Task $rank Core $CPU_ID GPU $ROCR_VISIBLE_DEVICES"
exec $*