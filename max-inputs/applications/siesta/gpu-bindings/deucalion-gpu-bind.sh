#!/bin/bash
CPU_ID=$(cat /proc/self/stat | awk '{print $39}')

limit0=40
limit1=48
limit2=104
limit3=112

if [ $CPU_ID -lt $limit0 ]
then
   export CUDA_VISIBLE_DEVICES=0

elif [ $CPU_ID -lt $limit1 ]
then
   export CUDA_VISIBLE_DEVICES=1

elif [ $CPU_ID -lt $limit2 ]
then
   export CUDA_VISIBLE_DEVICES=2

else
   export CUDA_VISIBLE_DEVICES=3

fi

echo "CPU $CPU_ID GPU $CUDA_VISIBLE_DEVICES"
exec $*