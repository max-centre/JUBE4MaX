#!/bin/bash

dout=$( realpath $PWD )/memreport
lock=${dout}/.lock
exe=$( realpath $0 )

# Define mode start/on/1 || stop/off/0
[ $# -eq 0 ] && mode=on || mode=${1,,}

# If start mode => turn on daemons
[[ "$mode" == "start" || "$mode" == "on" || "$mode" == "1" ]] && {
    # Remove the output directory
    [ -d ${dout} ] && rm -rf ${dout}

    # Create it again and generate a lock file
    mkdir ${dout} &> /dev/null && touch ${lock}

    # Get the list of host
    [ -z "${SLURM_JOB_NODELIST}" ] && SLURM_JOB_NODELIST="localhost"
    hosts=$( scontrol show hostnames $SLURM_JOB_NODELIST )

    # Turn on daemon
    for host in ${hosts}; do
        [ "${host}" == "localhost" ] && {
            [ -z "$i" ] && i=0 || i=$(( i+1 ))
            fout=${dout}/${host}.${i}.log
        } || fout=${dout}/${host}.log
        ssh ${host} "nohup ${exe} daemon ${lock}> ${fout} 2> /dev/null & disown"
    done 2> /dev/null
    exit
}

# If stop => remove lock file
[[ "$mode" == "stop" || "$mode" == "off" || "$mode" == "0" ]] && {
    [ -f "${lock}" ] && rm ${lock}
    exit
}

[[ "$mode" == "daemon" || "$mode" == "2" ]] && {
    [ $# -ge 2 ] && lock=$2
    while [ -f ${lock} ]; do
        echo $( date  +"%X" ) $( grep -e MemFree: -e Active: /proc/meminfo | rev | cut -d" " -f 2 | rev )
        sleep 2
    done
    exit
}
echo "# ERROR: Something is wrong in $0"
