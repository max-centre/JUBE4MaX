<?xml version="1.0" encoding="UTF-8"?>
<jube>
  <!-- Things needed as environment: account, binary path, lib paths -->

  <include-path>
    <!-- Machine specs -->
    <path tag="ariodante | hobsyllwin">${J4M_HOME}/platforms/ariodante</path>
    <path tag="vega+!gpu">${J4M_HOME}/platforms/sling/vegacpu</path>
    <path tag="vega+gpu">${J4M_HOME}/platforms/sling/vegagpu</path>
    <path tag="lumi+!gpu">${J4M_HOME}/platforms/lumi/lumi-cpu</path>
    <path tag="lumi+gpu">${J4M_HOME}/platforms/lumi/lumi-gpu</path>
    <path tag="karolina+!gpu">${J4M_HOME}/platforms/it4i/karolina-cpu</path>
    <path tag="karolina+gpu">${J4M_HOME}/platforms/it4i/karolina-gpu</path>
    <path tag="marenostrum+!gpu">${J4M_HOME}/platforms/marenostrum/MN5GPP</path>
    <path tag="marenostrum+gpu">${J4M_HOME}/platforms/marenostrum/MN5ACC</path>
    <path tag="leonardo+!gpu">${J4M_HOME}/platforms/cineca/leonardo/dcgp</path>
    <path tag="leonardo+gpu">${J4M_HOME}/platforms/cineca/leonardo/booster</path>
    <path tag="deucalion+!gpu">${J4M_HOME}/platforms/deucalion/deucalion-cpu</path>
    <path tag="deucalion+gpu">${J4M_HOME}/platforms/deucalion/deucalion-gpu</path>
    <path tag="meluxina+!gpu">${J4M_HOME}/platforms/meluxina/meluxina-cpu</path>
    <path tag="meluxina+gpu">${J4M_HOME}/platforms/meluxina/meluxina-gpu</path>
    <path tag="discoverer">${J4M_HOME}/platforms/discoverer</path>

    <!-- Deployment options (account,environment,etc) -->
    <path>${J4M_HOME}/applications/siesta/deployment</path>

    <!-- Workloads -->
    <path>${J4M_HOME}/workloads/siesta/si-quantum-dot/</path>
  </include-path>

  <!-- This reads the default values from deploy.xml. Account should probably -->
  <!-- as an environment variable. -->
  <parameterset name="deployParameter" init_with="deploy.xml">
  </parameterset>

  <!-- Small Silicon Bands benchmark for small PCs -->
  <benchmark name="ELSI CPU timings" outpath="_ELSI-DCGP">

    <parameterset name="systemParameter" init_with="platform.xml">
      <!-- no. of nodes, tasks, etc -->
      <parameter name="timelimit" update_mode="step">00:20:00</parameter>
      <parameter name="nodes"          type="int" >2, 4</parameter>
      <parameter name="taskspernode"   type="int" >24, 32</parameter>
      <parameter name="threadspertask" type="int" >1</parameter>
    	<parameter name="ncpus"          type="int" mode="python">$taskspernode*$nodes*$threadspertask</parameter>
	    <parameter name="ncores"         type="int" mode="python">$taskspernode*$nodes*$threadspertask</parameter>
      <parameter name="executable" tag="!gpu">siesta</parameter>
      <parameter name="executable" tag="gpu">$gpubindfile siesta</parameter>
      <parameter name="args_executable"> $fin > $fout </parameter>
    </parameterset>

    <parameterset name="testcaseset" init_with="workload.xml">
      <parameter name="basis_size"> DZP</parameter>
      <parameter name="max_iterations"> 1 </parameter>
      <parameter name="eigenstates_line">number-of-eigenstates -20 </parameter>
      <parameter name="solution_method" > elsi </parameter>
      <parameter name="elsi_solver"     > elpa </parameter>
      <parameter name="elpa_flavor"     > 2 </parameter>
      <parameter name="gpu_flag"        tag="gpu"> 1,0    </parameter>

      <parameter name="gpu_flag"        tag="!gpu"> 0            </parameter>

      <parameter name="tasks_per_pole"> 16 </parameter>
    </parameterset>

    <substituteset name="fdfsub" init_with="workload.xml">
    </substituteset>

    <!-- files needed incl job file and input files -->
    <fileset name="jobfiles" init_with="platform.xml">
      <copy tag="gpu"> ${J4M_HOME}/applications/siesta/gpu-bindings/$gpubindfile </copy>
      <copy>${J4M_HOME}/workloads/siesta/$dirin/*</copy>
    </fileset>

    <!-- Imports things needed from platform. -->
    <parameterset name="executeset" init_with="platform.xml">
      <parameter name="submit" tag="dry-run">echo</parameter>
      <!-- 
      <parameter name="modules"> $environ_load  </parameter>
      <parameter name="wrappre"> $siestaexports </parameter> 
      -->
      <parameter name="modules"> ml spack  </parameter>
      <parameter name="wrappre"> spack load siesta@5.2.0-beta-1 ~elpa +elsi_with_pexsi ~cuda </parameter> 

      <parameter name="mapping" tag="deucalion+gpu" > --cpu-list $default_bind </parameter>
      <parameter name="args_starter" tag="deucalion+gpu" > -np $tasks $mapping </parameter>
    </parameterset>

    <substituteset name="executesub" init_with="platform.xml">
      <sub source="@COMMA" dest="," />
    </substituteset>

    <!-- Operation -->
    <step name="submit"  >
      <use>deployParameter</use>
      <use>testcaseset</use>
      <use>systemParameter</use>
      <use>executeset</use>
      <use>jobfiles</use>
      <use>executesub</use>
      <use>fdfsub</use>
      <do>$submit $submit_script</do> <!-- actual submission directive -->
    </step>

    <!-- Postprocessing analysis -->
    <patternset name="pattern">
      <pattern name="global" type="float"></pattern>
      <pattern name="setup_h0" type="float">$jube_pat_bl Setup_H0 $jube_pat_bl $jube_pat_nint $jube_pat_bl ${jube_pat_fp}</pattern>
      <pattern name="setup_h" dotall="true" type="float">IterSCF.*?setup_H $jube_pat_bl $jube_pat_nint $jube_pat_bl ${jube_pat_fp}</pattern>
      <pattern name="nlefsm" type="float">$jube_pat_bl nlefsm $jube_pat_bl $jube_pat_nint $jube_pat_bl ${jube_pat_fp}</pattern>
      <pattern name="dhscf_init" type="float">$jube_pat_bl DHSCF_Init $jube_pat_bl $jube_pat_nint $jube_pat_bl ${jube_pat_fp}</pattern>
      <pattern name="overlap" type="float">$jube_pat_bl overlap $jube_pat_bl $jube_pat_nint $jube_pat_bl ${jube_pat_fp}</pattern>
      <pattern name="phion" type="float">$jube_pat_bl PHION  $jube_pat_bl $jube_pat_nint $jube_pat_bl ${jube_pat_fp}</pattern>
      <pattern name="poison" type="float">$jube_pat_bl POISON  $jube_pat_bl $jube_pat_nint $jube_pat_bl ${jube_pat_fp}</pattern>
      <pattern name="cholesky" dotall="true" type="float">
	Cholesky decomposition.*?Time : $jube_pat_bl ${jube_pat_fp}</pattern>
      <pattern name="transf_to_std" dotall="true" type="float">
	transformation to standard eigenproblem.*?Time : $jube_pat_bl ${jube_pat_fp}</pattern>
      <pattern name="solving_std" dotall="true" type="float">
	solving standard eigenproblem.*?Time : $jube_pat_bl ${jube_pat_fp}</pattern>
      <pattern name="back_transf" dotall="true" type="float">
      back-transformation of eigenvectors.*?Time : $jube_pat_bl ${jube_pat_fp}</pattern>
      <pattern name="dm_calc" dotall="true" type="float">
      density matrix calculation.*?Time : $jube_pat_bl ${jube_pat_fp}</pattern>
    </patternset>

    <analyser name="analyse">
      <use>pattern</use>
      <analyse step="submit">
        <file>$fout</file>
      </analyse>
    </analyser>

    <result>
      <use>analyse</use>
      <table name="result" style="pretty" sort="nodes" tag="!csv">
        <column title="Ncpus">ncpus</column>
        <column title="BasisSize">basis_size</column>
        <column title="ELPA alg">elpa_flavor</column>
        <column format=".2f" title="Setup_H0">setup_h0</column>
        <column format=".2f" title="nlefsm">nlefsm</column>
        <column format=".2f" title="DHSCF_Init">dhscf_init</column>
        <column format=".2f" title="Setup_H">setup_h</column>
        <column format=".2f" title="Cholesky">cholesky</column>
        <column format=".2f" title="Transf to Std">transf_to_std</column>
        <column format=".2f" title="Solving Std">solving_std</column>
        <column format=".2f" title="Back Transf">back_transf</column>
        <column format=".2f" title="DM Calc">dm_calc</column>
      </table>
   </result>
  </benchmark>
</jube>
