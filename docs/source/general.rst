General information on JUBE
==========================

.. _general


The full documentation can be found on the `JUBE website <https://apps.fz-juelich.de/jsc/jube/jube2/docu/index.html#>`_ 

How to install JUBE
---------------

Requirements: Python > 3.2

Download from `here <https://www.fz-juelich.de/en/ias/jsc/services/user-support/jsc-software-tools/jube/download>`_

Install with: 

.. code-block:: console 
   
   python setup.py install --user

JUBE commands
---------------

.. code-block:: console

   jube {run, continue, analyse, result} <inputfile>

* run : launches the benchmark 
* continue : continues the benchmark
* analyse : parses the outputs
* result : prints the table with results across workpackages 

.. autosummary::
   :toctree: generated
