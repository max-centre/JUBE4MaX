The MaX ecosystem
=================

.. _maxeco

The folder contains the following directories:

* platforms
* applications
* workloads
* scripts
* interface

with JUBE scripts and input files specific for a given *context* of the benchmark.

Platforms
---------------

The *platforms* folder is organized in directories specific for a given cluster. Each folder contains two scripts

* platform.xml : the JUBE script with platform-specific details 
* submit.slurm : the job script, with patterns to be substituted
  
The script platform.xml contains two parameter sets and the subsitute tag to replace patterns in the jobfile 

.. list-table:: tags in platform.xml
   :widths: 50 50
   :header-rows: 1

   * - Name
     - Purpose
   * - executeset
     - contains job-related information, e.g. the mpi launcher, modules, wrappers, environment variables.
   * - systemParameter
     - contains hardware-related parameters, e.g. the number of socket, gpus per node, cores per node, for derived parameters to be used in the job file
   * - executesub
     - defines the patterns to replace with executeset or systemParameter parameters in the jobfile
   * - jobfiles
     - is the copy tag to copy the jobfile inside the workpackage directory 

Our environment currently supports the following platforms

.. list-table:: platforms supported 
   :widths: 50 50
   :header-rows: 1

   * - Site
     - Partition
   * - CINECA
     - Leonardo, G100, Marconi
   * - CSC
     - Lumi, standard-g
   * - BSC
     - MareNostrumIV
   * - IT4I
     - Barbora, Power10


Applications
---------------

This folder contains the script application.xml with application specific tags such as

* executable and executable arguments;
* patterns for the analysis of the logfile
* tables for the presentation of results

Workloads
---------------

This folder containts a library of test cases for the different applications. Each subfolder is composed by

* inputfiles : a folder with all the inputs needed by the test case. The content will be copyed in each workpackage
* workload.xml : a xml script with the parameters of the inputfile and related substitution step

The content of inputfiles folder depends on the application. The files in inputfiles will be copied in the workpackage directory.

The workload.xml contains the following tabs

.. list-table:: tags in workload.xml
   :widths: 50 50
   :header-rows: 1

   * - Name
     - Purpose
   * - testcaseset
     - contains information related to the input, e.g. the name of the file, the verbosity, the amount of disk io, the number of steps
   * - inputsub
     - contains the definition of the patterns to substitute in the inputfile
   * - inputfiles
     - defines the tag to copy inputfiles in the workpackage directory

Scripts
---------------

Interface
---------------

The folder common contains the latest xml script. We are progressively achieving a level of generality where this benchmark file is indipendent from the application, platform, workload and eventually the tool used for profiling. 

By now, only the step for job submission, analysis and result tags are agnostic.
